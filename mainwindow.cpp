#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "openmeshdialog.h"
#include <vtkVersion.h>
#include <vtkConfigure.h>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkCellData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTetra.h>
#include <vtkThreshold.h>
#include <vtkDataSetMapper.h>
#include <vtkLookupTable.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCamera.h>
#include "tetrmeshreader.h"
#include <QtDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setDummyMesh();
    updateView();

    connect(ui->actionLoadMesh, &QAction::triggered,
            this, &MainWindow::loadMeshDialog);

    connect(ui->qualityRangeButton, &QPushButton::clicked,
            this, &MainWindow::applyQualityFilter);

    connect(ui->qualityButton, &QPushButton::clicked,
            this, &MainWindow::setQualityMeasure);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setMesh(vtkSmartPointer<vtkUnstructuredGrid> &mesh)
{
    this->filteredMesh = nullptr;
    this->mesh = mesh;
}

void MainWindow::setQualityMeasure()
{
    switch (ui->qualityComboBox->currentIndex()) {
    case 0:
        meshQualityMeasure = VTK_QUALITY_EDGE_RATIO;
        break;
    case 1:
        meshQualityMeasure = VTK_QUALITY_ASPECT_RATIO;
        break;
    case 2:
        meshQualityMeasure = VTK_QUALITY_RADIUS_RATIO;
        break;
    case 3:
        meshQualityMeasure = VTK_QUALITY_ASPECT_FROBENIUS;
        break;
    case 4:
        meshQualityMeasure = VTK_QUALITY_MIN_ANGLE;
        break;
    case 5:
        meshQualityMeasure = VTK_QUALITY_COLLAPSE_RATIO;
        break;
    case 6:
        meshQualityMeasure = VTK_QUALITY_ASPECT_BETA;
        break;
    case 7:
        meshQualityMeasure = VTK_QUALITY_ASPECT_GAMMA;
        break;
    case 8:
        meshQualityMeasure = VTK_QUALITY_VOLUME;
        break;
    case 9:
        meshQualityMeasure = VTK_QUALITY_CONDITION;
        break;
    case 10:
        meshQualityMeasure = VTK_QUALITY_JACOBIAN;
        break;
    case 11:
        meshQualityMeasure = VTK_QUALITY_SCALED_JACOBIAN;
        break;
    case 12:
        meshQualityMeasure = VTK_QUALITY_SHAPE;
        break;
    case 13:
        meshQualityMeasure = VTK_QUALITY_RELATIVE_SIZE_SQUARED;
        break;
    case 14:
        meshQualityMeasure = VTK_QUALITY_SHAPE_AND_SIZE;
        break;
    case 15:
        meshQualityMeasure = VTK_QUALITY_DISTORTION;
        break;
    default:
        qDebug() << "Unexpected quality measure" << "\n";
        break;
    }
}

double MainWindow::getCellQualityRatio(vtkCell* cell)
{
    switch (meshQualityMeasure) {
        case VTK_QUALITY_EDGE_RATIO:
            return vtkMeshQuality::TetEdgeRatio(cell);
        case VTK_QUALITY_ASPECT_RATIO:
            return vtkMeshQuality::TetAspectRatio(cell);
        case VTK_QUALITY_RADIUS_RATIO:
            return vtkMeshQuality::TetRadiusRatio(cell);
        case VTK_QUALITY_ASPECT_FROBENIUS:
            return vtkMeshQuality::TetAspectFrobenius(cell);
        case VTK_QUALITY_MIN_ANGLE:
            return vtkMeshQuality::TetMinAngle(cell);
        case VTK_QUALITY_COLLAPSE_RATIO:
            return vtkMeshQuality::TetCollapseRatio(cell);
        case VTK_QUALITY_ASPECT_BETA:
            return vtkMeshQuality::TetAspectBeta(cell);
        case VTK_QUALITY_ASPECT_GAMMA:
            return vtkMeshQuality::TetAspectGamma(cell);
        case VTK_QUALITY_VOLUME:
            return vtkMeshQuality::TetVolume(cell);
        case VTK_QUALITY_CONDITION:
            return vtkMeshQuality::TetCondition(cell);
        case  VTK_QUALITY_JACOBIAN:
            return vtkMeshQuality::TetJacobian(cell);
        case  VTK_QUALITY_SCALED_JACOBIAN:
            return vtkMeshQuality::TetScaledJacobian(cell);
        case  VTK_QUALITY_SHAPE:
            return vtkMeshQuality::TetShape(cell);
        case  VTK_QUALITY_RELATIVE_SIZE_SQUARED:
            return vtkMeshQuality::TetRelativeSizeSquared(cell);
        case  VTK_QUALITY_SHAPE_AND_SIZE:
            return vtkMeshQuality::TetShapeandSize(cell);
        case  VTK_QUALITY_DISTORTION:
            return vtkMeshQuality::TetDistortion(cell);
        default:
            qDebug() << "Unexpected quality measure" << "\n";
            break;
    }
}

void MainWindow::loadMeshDialog()
{
    OpenMeshDialog *openDialog = new OpenMeshDialog(this);
    connect(openDialog, &OpenMeshDialog::meshLoaded, this, &MainWindow::setMesh);
    openDialog->exec();
    updateView();
}

void MainWindow::applyQualityMap()
{
    //this->mesh->Print(std::cout);

    vtkSmartPointer<vtkFloatArray> cellData =
        vtkSmartPointer<vtkFloatArray>::New();

    minQuality = 0.0;
    maxQuality = 0.0;

    for (vtkIdType i = 0; i < this->mesh->GetNumberOfCells(); i++)
    {
        vtkSmartPointer<vtkCell> cell = this->mesh->GetCell(i);
        double quality = getCellQualityRatio(cell);
        cellData->InsertNextValue(quality);

        if (quality < minQuality) minQuality = quality;
        if (quality > maxQuality) maxQuality = quality;
    }
    //this->mesh->GetCellData()->RemoveArray(0);
    this->mesh->GetCellData()->SetScalars(cellData);
}

void MainWindow::applyQualityFilter()
{
    bool okMin = false;
    bool okMax = false;
    double min = ui->qualityMinLineEdit->text().toDouble(&okMin);
    double max = ui->qualityMaxLineEdit->text().toDouble(&okMax);

    if (!okMin || !okMax) {
        qDebug() << "Min/Max value parsing failed" << "\n";
        return;
    }

    vtkSmartPointer<vtkThreshold> threshold =
        vtkSmartPointer<vtkThreshold>::New();

    threshold->SetInputData(mesh);
    threshold->ThresholdBetween(min, max);
    threshold->Update();

    filteredMesh = threshold->GetOutput();
    updateView();
}

void MainWindow::updateView()
{
    this->applyQualityMap();

    vtkSmartPointer<vtkUnstructuredGrid> currentMesh =
        filteredMesh != nullptr ? filteredMesh : mesh;

    // Create the color map
    vtkSmartPointer<vtkLookupTable> colorLookupTable =
        vtkSmartPointer<vtkLookupTable>::New();

    colorLookupTable->SetTableRange(minQuality, maxQuality);
    colorLookupTable->Build();

    vtkSmartPointer<vtkDataSetMapper> mapper =
            vtkSmartPointer<vtkDataSetMapper>::New();
    mapper->SetInputData(currentMesh);
    mapper->SetScalarRange(minQuality, maxQuality);
    mapper->SetLookupTable(colorLookupTable);

    vtkSmartPointer<vtkActor> actor =
            vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->SetInterpolationToFlat();

    // Set the color for edges of the sphere
    actor->GetProperty()->SetEdgeColor(1.0, 0.0, 0.0); //(R,G,B)
    actor->GetProperty()->EdgeVisibilityOn();

    // VTK Renderer
    vtkSmartPointer<vtkRenderer> renderer =
        vtkSmartPointer<vtkRenderer>::New();
    renderer->AddActor(actor);

    // VTK/Qt wedded
    ui->viewWidget->GetRenderWindow()->AddRenderer(renderer);

    ui->qualityMinLineEdit->setText(QString::number(minQuality));
    ui->qualityMaxLineEdit->setText(QString::number(maxQuality));
}

void MainWindow::setDummyMesh()
{
    // Make a tetrahedron.
    int numberOfVertices = 4;

    vtkSmartPointer< vtkPoints > points =
        vtkSmartPointer< vtkPoints > :: New();
    points->InsertNextPoint(0, 0, 0);
    points->InsertNextPoint(1, 0, 0);
    points->InsertNextPoint(1, 1, 0);
    points->InsertNextPoint(0, 1, 1);

    vtkSmartPointer<vtkTetra> tetra =
        vtkSmartPointer<vtkTetra>::New();
    for (int i = 0; i < numberOfVertices; ++i)
    {
        tetra->GetPointIds()->SetId(i, i);
    }

    vtkSmartPointer<vtkCellArray> cellArray =
        vtkSmartPointer<vtkCellArray>::New();
    cellArray->InsertNextCell(tetra);

    mesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
    mesh->SetPoints(points);
    mesh->SetCells(VTK_TETRA, cellArray);
}
