#include "tetrmeshreader.h"
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkTetra.h>
#include <fstream>
#include <cerrno>
#include <cstdlib>
//#include <QDebug>

char* TetrMeshReader::fread2str(const char *fname, int& length)
{
    std::ifstream file;
    file.open(fname);
    file.seekg(0, std::ios::end);
    length = file.tellg();
    file.seekg(0, std::ios::beg);

    char *buffer = new char[length];
    file.read(buffer, length);
    file.close();
    return buffer;
}

vtkSmartPointer<vtkPoints> TetrMeshReader::parse_points(const char *fname, int num_points)
{
    vtkSmartPointer<vtkPoints> points
        = vtkSmartPointer<vtkPoints>::New();
    points->SetNumberOfPoints(num_points);

    int length = 0;
    char *buffer = fread2str(fname, length);

    errno = 0;
    char* next = nullptr;
    auto f = buffer;
    auto l = buffer + length;
    double x, y, z;
    vtkIdType i = 0;

    setlocale(LC_ALL, "C");

    while (errno == 0 && f && f<(l-12) )
    //for (vtkIdType i = 0; i < num_points/*, errno == 0, f < l*/; ++i)
    {
        x = strtod(f, &next); f = next;
        y = strtod(f, &next); f = next;
        z = strtod(f, &next); f = next;
        points->SetPoint(i, x, y, z);
        i++;
    }

    setlocale(LC_ALL, "");

    delete buffer;
    return points;
}

vtkSmartPointer<vtkCellArray> TetrMeshReader::parse_tetrs(const char *fname, int num_tetra)
{
    vtkSmartPointer<vtkCellArray> cellArray =
        vtkSmartPointer<vtkCellArray>::New();

    int length = 0;
    char *buffer = fread2str(fname, length);

    errno = 0;
    char* next = nullptr;
    auto f = buffer;
    auto l = buffer + length;
    double p0, p1, p2, p3;
    for (vtkIdType i = 0; i < num_tetra && errno == 0 && f < l; ++i)
    {
        p1 = strtod(f, &next); f = next;
        p0 = strtod(f, &next); f = next;
        p2 = strtod(f, &next); f = next;
        p3 = strtod(f, &next); f = next;

        vtkSmartPointer<vtkTetra> tetra =
            vtkSmartPointer<vtkTetra>::New();

        tetra->GetPointIds()->SetId(0, p0);
        tetra->GetPointIds()->SetId(1, p1);
        tetra->GetPointIds()->SetId(2, p2);
        tetra->GetPointIds()->SetId(3, p3);

        cellArray->InsertNextCell(tetra);
    }
    delete buffer;
    return cellArray;
}

vtkSmartPointer<vtkUnstructuredGrid> TetrMeshReader::read()
{
    std::ifstream fparams;
    fparams.open(params_path.c_str(), std::ifstream::in);
    vtkIdType num_points, num_tetra;
    fparams >> num_points >> num_tetra;
    // qDebug() << num_points << " " << num_tetra << "\n";

    vtkSmartPointer<vtkPoints> points =
        parse_points(point_path.c_str(), num_points);

    vtkSmartPointer<vtkCellArray> cellArray =
        parse_tetrs(tetra_path.c_str(), num_tetra);

    vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid =
        vtkSmartPointer<vtkUnstructuredGrid>::New();
    unstructuredGrid->SetPoints(points);
    unstructuredGrid->SetCells(VTK_TETRA, cellArray);

    return unstructuredGrid;
}
