# QtTetrMeshViewer
Qt 5 based tetrahedral mesh viewer

![fenestron](./doc/img/fenestron.png) ![fenestron2](./doc/img/fenestron2.png) ![gear](./doc/img/gear.png) ![gear2](./doc/img/gearfiltered.png)

## Building
This project uses CMake instead of QMake and depends on VTK library. Therefore for both QT and command line build the following command can be used:
```
cmake -DQT_QMAKE_EXECUTABLE:FILEPATH=/usr/something/qmake
      -DVTK_DIR:PATH=/usr/something/VTK/VTKBuild-qt5
      /path/to/source
```

## Roadmap
* Remember mesh file folder after used have chosen at least one file (or/and add mesh folder input field)
* Add busy bar to the Reader
* Implement VTK based mesh reader (e.g. by extending vtkDataReader)
* Add histogram navigation widget (culled cells should become opaque)
