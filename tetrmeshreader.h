#ifndef TETRMESHREADER_H
#define TETRMESHREADER_H

#include <string>
//#include <fstream>
#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>

class TetrMeshReader
{
public:
    TetrMeshReader(std::string& params_path,
                   std::string& point_path,
                   std::string& tetra_path,
                   std::string& surface_path)
                   : params_path(params_path),
                   point_path(point_path),
                   tetra_path(tetra_path),
                   surface_path(surface_path) {}

    vtkSmartPointer<vtkUnstructuredGrid> read();

private:
    std::string params_path;
    std::string point_path;
    std::string tetra_path;
    std::string surface_path;
    char* fread2str(const char *fname, int& length);

    vtkSmartPointer<vtkPoints> parse_points(const char *fname,
                                            int num_points);

    vtkSmartPointer<vtkCellArray> parse_tetrs(const char *fname,
                                              int num_tetra);
};

#endif // TETRMESHREADER_H
