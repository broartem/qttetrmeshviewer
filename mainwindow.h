#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkMeshQuality.h>
#include <vtkCell.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    vtkSmartPointer<vtkUnstructuredGrid> mesh = nullptr;
    vtkSmartPointer<vtkUnstructuredGrid> filteredMesh = nullptr;

    char meshQualityMeasure = VTK_QUALITY_EDGE_RATIO;
    double minQuality = 0.0;
    double maxQuality = 0.0;

    void setDummyMesh();
    void applyQualityMap();
    double getCellQualityRatio(vtkCell* cell);

private slots:
    void loadMeshDialog();
    void updateView();
    void applyQualityFilter();
    void setQualityMeasure();
    void setMesh(vtkSmartPointer<vtkUnstructuredGrid>& mesh);
};

#endif // MAINWINDOW_H
