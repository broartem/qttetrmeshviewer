#include "openmeshdialog.h"
#include "ui_openmeshdialog.h"
#include "tetrmeshreader.h"
#include <QString>
#include <QFileDialog>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>

OpenMeshDialog::OpenMeshDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenMeshDialog)
{
    ui->setupUi(this);

    connect(ui->paramFileButton, &QPushButton::clicked,
            this, &OpenMeshDialog::openParamsFileDialog);

    connect(ui->paramFileButton, &QPushButton::clicked,
            this, &OpenMeshDialog::openCoordsFileDialog);

    connect(ui->paramFileButton, &QPushButton::clicked,
            this, &OpenMeshDialog::openTetraFileDialog);

    connect(ui->buttonBox, &QDialogButtonBox::accepted,
            this, &OpenMeshDialog::loadMesh);
}

OpenMeshDialog::~OpenMeshDialog()
{
    delete ui;
}

void OpenMeshDialog::openParamsFileDialog()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Choose Mesh Parameters File"), "");
    ui->paramFilePath->setText(fileName);
}

void OpenMeshDialog::openCoordsFileDialog()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Choose Mesh Coordinates File"), "");
    ui->coordsFilePath->setText(fileName);
}

void OpenMeshDialog::openTetraFileDialog()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Choose Mesh Tetrahedra File"), "");
    ui->tetraFilePath->setText(fileName);
}

void OpenMeshDialog::openSurfaceTrisFileDialog()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Choose Mesh Surface Triangles File"), "");
    ui->trisFilePath->setText(fileName);
}

void OpenMeshDialog::loadMesh()
{
    std::string paramFilePath = ui->paramFilePath->text().toStdString();
    std::string coordsFilePath = ui->coordsFilePath->text().toStdString();
    std::string tetraFilePath = ui->tetraFilePath->text().toStdString();
    std::string trisFilePath = ui->trisFilePath->text().toStdString();

    TetrMeshReader *reader = new TetrMeshReader(paramFilePath,
                                                coordsFilePath,
                                                tetraFilePath,
                                                trisFilePath);
    vtkSmartPointer<vtkUnstructuredGrid> mesh = reader->read();
    emit meshLoaded(mesh);
    this->accept();
}
