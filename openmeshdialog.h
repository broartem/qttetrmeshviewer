#ifndef OPENMESHDIALOG_H
#define OPENMESHDIALOG_H

#include <QDialog>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>

namespace Ui {
class OpenMeshDialog;
}

class OpenMeshDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OpenMeshDialog(QWidget *parent = 0);
    ~OpenMeshDialog();

signals:
    void meshLoaded(vtkSmartPointer<vtkUnstructuredGrid>& mesh);

private:
    Ui::OpenMeshDialog *ui;

private slots:
    void openParamsFileDialog();
    void openCoordsFileDialog();
    void openTetraFileDialog();
    void openSurfaceTrisFileDialog();
    void loadMesh();
};

#endif // OPENMESHDIALOG_H
